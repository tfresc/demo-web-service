#list de 3 entiers 

numbers = [23, 140, 0]
print(type(numbers)) #liste 

for n in numbers:
    print(n)
    computedIndex = 1 + 1
    print(numbers[computedIndex])

count = 0
while count < 3:
    print(numbers[count])
    count += 1

title = "les trois mousquetaires"
print(title[4])
for c in title:
    print(c)

acc = 0 #variable accumulateur
search = "s"
for c in title:
    if c == search:
        acc += 1
print("le caractere %s a été trouvé %d fois" % (search, acc))