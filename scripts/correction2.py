from sys import argv

n = int(argv[1])

if n < 0 or n > 1000:
    print("doit etre compris entre 0 et 1000")
    exit(1)

for i in range(11):
    print("%s x %d = %d" % (n, i, n * i))
