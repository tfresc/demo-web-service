t = ()
print(type(t)) #tupple

coordsGps = (45.9632, 21.7891)
print(coordsGps)
print("latitude", coordsGps[0])
print("longitude", coordsGps[1])

#coordsGps[0] = 58555 attention tupple ne permet pas l'ecrasement des donnes
# l = [12,13]
# print(l)
# l.append(14) #ajoute un element a la liste possible dans tableau []
# print(l)
#coordsGps.append(896.2558) IMPOSSIBLE TUPPLE NE PERMET NI AJOUT NI MODIF
lat, lng = coordsGps
print(lat, lng)