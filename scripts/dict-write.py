student1= {
    "age": 25,
    "laname": "PASCAL",
    "fname": "Blaise",
    "isGoodlearner": True,
    }

print(student1)
#ajout dune clé valeur
student1["grades"] = [17, 12, 19.5]
print(student1)
#modification d'une valeur 
student1["age"] = 27
print(student1)
#modif multi clés
student1.update({"age":22,"fname":"blaisou"})
print(student1)
#supression d'une clé
student1.pop("grades")
print(student1)
#iteration sur les valeurs du dict uniquement
for v in student1.values():
    print(v)
for k in student1.keys():
    print(k)
for k,v in student1.items():
    print(k, v)