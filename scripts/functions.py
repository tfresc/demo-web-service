def hello():
    print("Hello")
#return none

def hello2():
    return "Hello again"
def addtwo(n):
    return n + 2
# hello2() #inutile de le laisser l'appelle de la fonction ne serta a rien
# print(hello2()) #voila comment appeler la focntion au sein d'une commande ici print
def square(n):
    return n * n
def sum(a,b):
    return a + b

r = hello() #recoit none
print(r)

print(addtwo(8))
finalValue = addtwo(addtwo(addtwo(5)))# 5 => 7 => 9 => 11
print(finalValue)

print(square(5))
print(sum(4,1) + sum(6,4))

#affichage du carré pour toutes les valeurs itérés pour les valeurs supérieur à 10
numbers =[6,4,40,10,8,15]

for n in numbers:
    if n >=10:
        print(square(n))
        print (n, "=>", square(n))