# Variables primitives en Python
temperature = 15
pi = 3.14
isearthRound = True
training = "inititation au langage python"

#affichage du type de variables

print(type(temperature)) #int
print(type(pi)) #float
print(type(isearthRound)) #booleen
print(type(training)) #string

# print(2+2)
# print(temperature + temperature)
# training = "perfectionnement au langage"
# print(training)

print(temperature + 10) # addition entre deux vairables entier
print(training + " 10") #concatenation entre deuxchaines
print(pi + 10) #addition entre float et int 
print(isearthRound + 10) #conversion implicite true est egal a 1
print("le double de pi est:" + str(pi * 2)) #fction de conversion str ()

doublepi = pi * 2 #stockage en variable du retrour d'une expression arythmetique
print(type(doublepi)) #float
doublepistr = str(doublepi)
print(type(doublepistr)) #str
