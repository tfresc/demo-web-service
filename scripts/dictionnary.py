d = {}
print(type(d)) #type dict

student1= {
    "age": 25,
    "laname": "PASCAL",
    "fname": "Blaise",
    "isGoodlearner": True,
    "grades": [17,12,19.5]
}

student2= {
    "age": 36,
    "laname": "DEL PIERO",
    "fname": "Alessandro",
    "isGoodlearner": False,
    "grades": [10, 10, 10]
}

student3= {
    "age": 58,
    "laname": "TOURGE",
    "fname": "Ivan",
    "isGoodlearner": True,
    "grades": [19, 7.5, 2.5]
}

#affichage de la premiere note obtenue par student1

print(student1["grades"][0])

#Iteration sur clés dictionnaires
for k in student1:
    print(k,"=>", student1[k])

#liste de dictionnaires
students = [student1,student2,student3]
evaluation = "bon"
for s in students:
    if not s["isGoodlearner"]:
        evaluation = "mauvais"
    else:
        evaluation = "bon"
    print("%s est un %s etudiant" % (s["fname"], evaluation))