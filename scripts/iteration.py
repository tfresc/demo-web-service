#boucle while
count = 1

while count <= 5:
    print("bonjour")
    count += 1 #incrementation de 1

count2 = 5

while count2 > 0:
    print("Bye", count2)
    count2 -= 1 #decrémentation de 1

#boucle for ... in 
for n in range(100):
    print("coucou", n)
    if n == 5:
        break
